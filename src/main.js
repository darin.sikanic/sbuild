import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
// import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome,
  faCloud,
  faRocket,
  faLightbulb,
  faEnvelope,
  faPlus,
  faExternalLinkAlt,
  faGlobe,
} from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faLinkedin, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './styles/main.scss'

library.add(faHome,
  faCloud,
  faRocket,
  faLightbulb,
  faEnvelope,
  faPlus,
  faExternalLinkAlt,
  faGlobe,
  faFacebook,
  faLinkedin,
  faTwitter,
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(BootstrapVue);
new Vue({
  render: h => h(App)
}).$mount('#app')
